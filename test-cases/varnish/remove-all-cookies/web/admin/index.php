<?php
/**
 *
 * This file is part of the ZerusTech HTTP Cache Tutorial package.
 * 
 * (c) Michael Lee <michael.lee@zerustech.com>
 *
 * For the full copyright and license information, please view the LICENSE file 
 * that was distributed with this source code.
 *
*/

/**
 * Default server side test script.
 *  
 * @author Michael Lee <michael.lee@zerustech.com>
 */
$now = gmdate('D, d M Y H:i:s \G\M\T');

header('Cache-Control: max-age=5');

printf('<h1>Admin response from %s:%s</h1>', $_SERVER['HTTP_HOST'], $_SERVER['SERVER_PORT']);

printf('<h1>Cookie: %s</h1>', (isset($_SERVER['HTTP_COOKIE']) ? $_SERVER['HTTP_COOKIE'] : ''));

printf('<h1>System Time: %s</h1>', $now);
