vcl 4.0;

/**
 * This file is part of the ZerusTech package.
 *
 * (c) Michael Lee <michael.lee@zerustech.com>
 *
 * For the full copyright and license information, please view the LICENSE file
 * that was distributed with this source code.
*/

import std;

/**
 * This vcl adds support for request Cache-Control directives. By default,
 * Varnish ignores all Cache-Control directives.
 * In fact, the behavior here does not comform to RFC7234 either, because it
 * simply ignores varnish cache. The expected behavior should be re-validating
 * the cache upon every single request, but I didn't find a way to configure
 * varnish to do that.
 *
 * NOTE:
 * Varnish compiles VCL code from top to bottom, and finally the built-in VCL
 * code, and the code for the same subroutine will be appended in the same order
 * as they are defined. So in this case, vcl_recv should be defined before the
 * inclusion of config.vcl.  And if there is a return() in the subroutine, the
 * process will terminate, and the code below will not be executed.
 * Refer to
 * https://www.getpagespeed.com/server-setup/varnish/varnish-virtual-hosts.
 * I can't believe that this explanation is missing from the official
 * documentation of Varnish.
 *
 * @author Michael Lee <michael.lee@zerustech.com>
*/
sub vcl_recv {

    // The no-cache, no-store, private, max-age, and pragma directives.
    if (req.http.Cache-Control ~ "(no-cache|no-store|private|max-age=0)" || req.http.Pragma == "no-cache") {

        return(pass);
    }
}

include "../../../../default/etc/config.vcl";
